function onLoad(){
    $("#btnCalculate").click(function(e){

        if ($("#c_v").val() < 15) {
            alert("There must be at least 15 control trials for this tool to produce any results.");
            return;
        }
        if ($("#v_v").val() < 15) {
            alert("There must be at least 15 variation trials for this tool to produce any results.");
            return;
        }

        e.preventDefault();
        var data = {
            control_visitors: $("#c_v").val(),
            variation_visitors: $("#v_v").val(),
            control_conversions: $("#c_c").val(),
            variation_conversions: $("#v_c").val()
        }

        $.ajax({
            url: "/api/calculate",
            method: "POST",
            data: data,
            dataType: "json",
            success: function(data){
                var pValue = data.p_value;
                var isSignificant = data.is_significant;

                if(pValue === -1){
                  $("#pValue").text("N/A");
                }else{
                  $("#pValue").text(pValue);
                }

                if(isSignificant === "true"){
                  $("#isSignificant").text("Yes");
                }else{
                  $("#isSignificant").text("No");
                }

            }
        });
    });
}
