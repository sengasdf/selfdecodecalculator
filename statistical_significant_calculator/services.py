import math

from statistical_significant_calculator.models import CalculatorOutput


class Calculator:
    @staticmethod
    def calculate(calculator_input):
        c_v = calculator_input.control_visitors
        v_v = calculator_input.variation_visitors
        c_c = calculator_input.control_conversions
        v_c = calculator_input.variation_conversions

        c_p = c_c / c_v
        v_p = v_c / v_v

        try:
            std_error = math.sqrt((c_p * (1 - c_p) / c_v) + (v_p * (1 - v_p) / v_v))
            z_value = (v_p - c_p) / std_error
            p_value = Calculator.normal_p(z_value);
            if p_value > 0.5:
                p_value = 1 - p_value
            p_value = round(p_value * 1000) / 1000

            if p_value < 0.05:
                is_significant = True
            else:
                is_significant = False
        except (ZeroDivisionError, ValueError) as e:
            p_value = -1
            is_significant = False

        return CalculatorOutput(p_value=p_value, is_significant=is_significant)

    @staticmethod
    def normal_p(x):
        d1 = 0.0498673470
        d2 = 0.0211410061
        d3 = 0.0032776263
        d4 = 0.0000380036
        d5 = 0.0000488906
        d6 = 0.0000053830

        a = abs(x)
        t = 1.0 + a * (d1 + a * (d2 + a * (d3 + a * (d4 + a * (d5 + a * d6)))))
        t *= t
        t *= t
        t *= t
        t *= t
        t = 1.0 / (t + t)
        if x >= 0:
            t = 1 - t
        return t
