from rest_framework import serializers

from statistical_significant_calculator.models import CalculatorInput, CalculatorOutput


class CalculatorInputSerializer(serializers.Serializer):
    control_visitors = serializers.IntegerField(min_value=15)
    variation_visitors = serializers.IntegerField(min_value=15)
    control_conversions = serializers.IntegerField()
    variation_conversions = serializers.IntegerField()

    def create(self, validated_data):
        return CalculatorInput(**validated_data)


class CalculatorOutputSerializer(serializers.Serializer):
    p_value = serializers.IntegerField()
    is_significant = serializers.BooleanField()

    def create(self, validated_data):
        return CalculatorOutput(**validated_data)
