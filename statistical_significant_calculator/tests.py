from random import randint

# Create your tests here.
import itertools
from rest_framework.test import APITestCase


class CalculatorTestCase(APITestCase):
    def test_calculate(self):
        for _ in itertools.repeat(None, 1000):
            control_visitors = randint(15, 1000)
            variation_visitors = randint(15, 1000)
            control_conversions = randint(0, 1000)
            variation_conversions = randint(0, 1000)

            # print(control_visitors)
            # print(variation_visitors)
            # print(control_conversions)
            # print(variation_conversions)

            response = self.client.post("/calculate", {
                "control_visitors": control_visitors,
                "variation_visitors": variation_visitors,
                "control_conversions": control_conversions,
                "variation_conversions": variation_conversions
            })
            self.assertEqual(response.status_code, 200)
