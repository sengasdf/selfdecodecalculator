from django.conf.urls import url

from statistical_significant_calculator import views

urlpatterns = [
    url(r'^calculate', views.calculate),
]
