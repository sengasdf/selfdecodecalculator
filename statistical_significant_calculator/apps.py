from django.apps import AppConfig


class StatisticalSignificantCalculatorConfig(AppConfig):
    name = 'statistical_significant_calculator'
