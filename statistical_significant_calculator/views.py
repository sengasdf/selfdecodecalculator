from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from statistical_significant_calculator import serializers
from statistical_significant_calculator.services import Calculator


@api_view(["POST"])
def calculate(request):
    data = request.data

    input_serializer = serializers.CalculatorInputSerializer(data=data)

    if not input_serializer.is_valid():
        return Response(input_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    input = input_serializer.create(input_serializer.validated_data)
    output = Calculator.calculate(input)

    output_serializer = serializers.CalculatorOutputSerializer(instance=output)

    return Response(output_serializer.data, status=status.HTTP_200_OK)
