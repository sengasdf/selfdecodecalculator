from django.db import models


# Create your models here.
class CalculatorInput:
    def __init__(self, **kwargs):
        self.control_visitors = kwargs.get("control_visitors", None)
        self.variation_visitors = kwargs.get("variation_visitors", None)
        self.control_conversions = kwargs.get("control_conversions", None)
        self.variation_conversions = kwargs.get("variation_conversions", None)


class CalculatorOutput:
    def __init__(self, **kwargs):
        self.p_value = kwargs.get("p_value", None)
        self.is_significant = kwargs.get("is_significant", None)
